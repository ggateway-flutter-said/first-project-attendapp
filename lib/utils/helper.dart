import 'package:first_project/widgets/app_text_widget.dart';
import 'package:flutter/material.dart';


mixin Helper {
  void showSnackBar(
      BuildContext context, {
        required String text,
        bool error = false,
        String? actionTitle,
        Function()? onPressed,
      }) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: AppTextWidget(
          text,
          color: Colors.white,
          size: 16,
          fontWeight: FontWeight.bold,
        ),
        backgroundColor: error ? Colors.red : Colors.blue,
        behavior: SnackBarBehavior.floating,
        margin: EdgeInsets.all(20),
        elevation: 5,
        duration: Duration(seconds: 3),
        action: actionTitle != null
            ? SnackBarAction(
          onPressed: onPressed ?? (){},
          label: actionTitle,
          textColor: Colors.yellow,
        )
            : null,
        onVisible: () {
          print('VISIBLE - SNACKBAR');
        },
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(
             10),
        )
        // padding: EdgeInsets.all(15),
      ),
    );
  }
}
