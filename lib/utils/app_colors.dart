// const
import 'package:flutter/material.dart';

class AppColors {
  ///GRADIENT: BEGIN_COLOR
  static const GRADIENT_BEGIN_COLOR = Color(0xFF8465FF);
  ///GRADIENT: END_COLOR
  static const GRADIENT_END_COLOR = Color(0xFF5933F1);

  static const TITLE_TEXT_COLOR = Color(0xFF23203F);
  static const SUB_TITLE_TEXT_COLOR = Color(0xFF9DA8C3);

  static const INDICATOR_DEFAULT_COLOR = Color(0xFF9DA8C3);
  static const INDICATOR_SELECTED_COLOR = Color(0xFF5933F1);
  static const SCAFFOLD_COLOR = Color(0xFFF2F6FC);
}
