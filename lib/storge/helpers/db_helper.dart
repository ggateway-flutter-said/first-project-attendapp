import 'dart:io';

import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DBHelper {
  //database name
  // static final String dbName = 'student.sql';

  ///student table
  // static final String studentTableName = 'student';
  // static final String studentIDColumnName = 'id';
  // static final String studentNameColumnName = 'name';
  // static final String studentAttendeesColumnName = 'attendees';
  // static final String studentImageColumnName = 'image';
  // static final String studentSubjectColumnName = 'courseId';

  ///course table
  // static final String subjectTableName = 'subject';
  // static final String subjectIDColumnName = 'id';
  // static final String subjectNameColumnName = 'name';
  // static final String subjectAttendDayNumberColumnName = 'days';
  // static final String subjectStudentNumberColumnName = 'attendees';

  static final DBHelper _instance = DBHelper._internal();
  late final Database _database;

  factory DBHelper() {
    return _instance;
  }

  DBHelper._internal();

  Database get database => _database;

  Future<void> initDatabase() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String path = join(directory.path, 'student.sql');
    _database = await openDatabase(
      path,
      version: 1,
      onOpen: (Database db) {},
      onCreate: (Database db, int version) async {
        createTables(db);
      },
      onUpgrade: (Database db, int oldVersion, int newVersion) {},
      onDowngrade: (Database db, int oldVersion, int newVersion) {},
    );
  }

  createTables(Database db) async{
    await db.execute('CREATE TABLE student ('
        'id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'name TEXT NOT NULL,'
        'attendees INTEGER NOT NULL,'
        'courseId INTEGER NOT NULL,'
        'gender INTEGER NOT NULL'
        ')');

    await db.execute('CREATE TABLE course ('
        'id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'name TEXT NOT NULL,'
        'days INTEGER NOT NULL,'
        'image TEXT'
        ')');
  }

}
