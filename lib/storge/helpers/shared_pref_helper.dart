import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefHelper {

  static final SharedPrefHelper _instance = SharedPrefHelper._internal();
  late final SharedPreferences _sharedPreferences;

  factory SharedPrefHelper(){
    return _instance;
  }

  SharedPrefHelper._internal();

  Future<void> initPref() async {
    _sharedPreferences = await SharedPreferences.getInstance();
  }

  Future<bool> setLanguage(String languageCode) async{
    return await _sharedPreferences.setString('language_code', languageCode);
  }

  String get languageCode {
    return _sharedPreferences.getString('language_code') ?? 'en';
  }


  Future<bool> setLoginState(bool isFirstTime) async{
    return await _sharedPreferences.setBool('isFirstTime', isFirstTime);
  }

  bool get loginState {
    return _sharedPreferences.getBool('isFirstTime') ?? true;
  }
}