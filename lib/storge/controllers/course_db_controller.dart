import 'package:first_project/models/course.dart';
import 'package:first_project/storge/abstract_layer/db_operations.dart';
import 'package:sqflite/sqflite.dart';

import '../helpers/db_helper.dart';

class CourseDbController implements DbOperations<Course> {
  Database _database;

  CourseDbController() : _database = DBHelper().database;

  @override
  Future<int> create(Course data) async {
    return await _database.insert('course', data.toMap());
  }

  @override
  Future<bool> delete(int id) async {
    int countOfDeletedRows =
        await _database.delete('course', where: 'id = ?', whereArgs: [id]);
    return countOfDeletedRows != 0;
  }

  @override
  Future<List<Course>> read() async {
    var rowsMaps = await _database.query('course');
    return rowsMaps.map((rowMap) => Course.fromMap(rowMap)).toList();
  }

  @override
  Future<Course?> show(int id) async {
    var data =
        await _database.query('course', where: 'id = ?', whereArgs: [id]);
    return data.isNotEmpty
        ? data
            .map(
              (rowMap) => Course.fromMap(rowMap),
            )
            .first
        : null;
  }

  @override
  Future<bool> update(Course data) async {
    int countOfUpdatedRows = await _database.update('course', data.toMapToUpdata(data),
        where: 'id = ?', whereArgs: [data.courseId]);
    return countOfUpdatedRows != 0;
  }
}
