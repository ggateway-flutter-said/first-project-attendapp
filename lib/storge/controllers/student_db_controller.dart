import 'package:first_project/storge/abstract_layer/db_operations.dart';
import 'package:first_project/models/student.dart';
import 'package:sqflite/sqflite.dart';

import '../helpers/db_helper.dart';

class StudentDbController implements DbOperations<Student> {
  Database _database;

  StudentDbController() : _database = DBHelper().database;

  @override
  Future<int> create(Student data) async {
    return await _database.insert('student', data.toMap());
  }

  @override
  Future<bool> delete(int id) async {
    int countOfDeletedRows = await _database
        .delete('student', where: 'id = ?', whereArgs: [id]);
    return countOfDeletedRows != 0;
  }

  @override
  Future<List<Student>> read() async {
    var rowsMaps = await _database.query('student');
    return rowsMaps.map((rowMap) => Student.fromMap(rowMap)).toList();
  }

  @override
  Future<Student?> show(int id) async {
    var data = await _database.query('student', where: 'id = ?', whereArgs: [id]);
    return data.isNotEmpty ? data.map((rowMap) => Student.fromMap(rowMap)).first : null;
  }

  @override
  Future<bool> update(Student data) async{
    int countOfUpdatedRows = await _database.update(
        'student', data.toMap(),
        where: 'id = ?', whereArgs: [data.studentId]);
    return countOfUpdatedRows != 0;
  }
}
