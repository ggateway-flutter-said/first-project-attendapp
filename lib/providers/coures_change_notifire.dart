import 'package:first_project/models/course.dart';
import 'package:first_project/storge/controllers/course_db_controller.dart';
import 'package:flutter/material.dart';

class CourseChangeNotifier extends ChangeNotifier {
  List<Course> courses = <Course>[];

  CourseChangeNotifier() {
    read();
  }
  Future<void> read() async {
    courses = await CourseDbController().read();
    notifyListeners();
  }

  Future<bool> create(Course data) async {
    int id = await CourseDbController().create(data);
    if (id != 0) {
      data.courseId = id;
      courses.add(data);
      notifyListeners();
    }
    return id != 0;
  }

  Future<bool> update(Course data) async {
    bool updated = await CourseDbController().update(data);
    if (updated) {
      int index = courses.indexWhere((course) => course.courseId == data.courseId);
      courses[index] = data;
      notifyListeners();
    }
    return updated;
  }

  Future<bool> delete(int id) async {
    bool deleted = await CourseDbController().delete(id);
    if (deleted) {
      courses.removeWhere((element) => element.courseId == id);
      notifyListeners();
    }
    return deleted;
  }
}
