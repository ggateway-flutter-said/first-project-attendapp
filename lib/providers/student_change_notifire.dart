import 'package:first_project/models/student.dart';
import 'package:first_project/storge/controllers/student_db_controller.dart';
import 'package:flutter/material.dart';

class StudentChangeNotifier extends ChangeNotifier {

  List<Student> students = <Student>[];

  StudentChangeNotifier() {
    read();
  }
  Future<void> read() async {
    students = await StudentDbController().read();
    notifyListeners();
  }



  Future<bool> create(Student data) async {
    int id = await StudentDbController().create(data);
    if (id != 0) {
      data.studentId = id;
      students.add(data);
      notifyListeners();
    }
    return id != 0;
  }

  Future<bool> update(Student data) async {
    bool updated = await StudentDbController().update(data);
    if (updated) {
      int index = students.indexWhere((contact) => contact.studentId == data.studentId);
      students[index] = data;
      notifyListeners();
    }
    return updated;
  }

  Future<bool> delete(int id) async {
    bool deleted = await StudentDbController().delete(id);
    if (deleted) {
      students.removeWhere((element) => element.studentId == id);
      notifyListeners();
    }
    return deleted;
  }
}
