import 'package:first_project/storge/helpers/shared_pref_helper.dart';
import 'package:flutter/material.dart';

class SettingsChangeNotifier extends ChangeNotifier {
  String languageCode = SharedPrefHelper().languageCode;

  void changeLang(String newLanguageCode) {
    SharedPrefHelper().setLanguage(newLanguageCode);
    languageCode = newLanguageCode;
    notifyListeners();
  }


}
