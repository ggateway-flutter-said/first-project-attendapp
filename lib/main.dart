import 'package:first_project/providers/coures_change_notifire.dart';
import 'package:first_project/providers/settings_change-notifire.dart';
import 'package:first_project/providers/student_change_notifire.dart';
import 'package:first_project/screens/add_course_screen.dart';
import 'package:first_project/screens/coures_screen.dart';
import 'package:first_project/screens/edite_course_screen.dart';
import 'package:first_project/screens/launch_screen.dart';
import 'package:first_project/screens/on_boarding_screen.dart';
import 'package:first_project/storge/helpers/shared_pref_helper.dart';
import 'package:first_project/storge/helpers/db_helper.dart';
import 'package:first_project/utils/app_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';


void main() async {
  WidgetsFlutterBinding();
  await DBHelper().initDatabase();
  await SharedPrefHelper().initPref();
  runApp(MainApp());
}

class MainApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<CourseChangeNotifier>(
            create: (_) => CourseChangeNotifier()),
        ChangeNotifierProvider<StudentChangeNotifier>(
            create: (_) => StudentChangeNotifier()),
        ChangeNotifierProvider<SettingsChangeNotifier>(
            create: (_) => SettingsChangeNotifier()),
      ],
      builder: (context, child){
        return MaterialApp(
          theme: ThemeData(
            primaryColor: AppColors.GRADIENT_BEGIN_COLOR
          ),
          localizationsDelegates: [
            AppLocalizations.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate
          ],
          supportedLocales: [
            const Locale('ar'),
            const Locale('en'),
          ],
          // locale:Locale('ar'),
          locale:Locale(Provider.of<SettingsChangeNotifier>(context).languageCode),
          initialRoute: '/launch_screen',
          routes: {
            '/launch_screen': (context) => LaunchScreen(),
            '/on_boarding_screen': (context) => OnBoardingScreen(),
            '/course_screen': (context) => CourseScreen(),
            '/add_course_screen': (context) => AddCourseScreen(),
            // '/edit_course_screen': (context) => EditCourseScreen(),
          },
        );
      },
    );
  }
}
