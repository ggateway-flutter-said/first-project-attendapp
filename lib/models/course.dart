import 'package:first_project/storge/helpers/db_helper.dart';
import 'package:flutter/material.dart';

class Course {
  late String courseName;
  late int courseId;
  late int numberOfDay;
  String? image;

  Course();

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['name'] = this.courseName;
    map['days'] = this.numberOfDay;
    map['image'] = this.image;
    return map;
  }


  Map<String, dynamic> toMapToUpdata(Course course) {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['id'] = course.courseId;
    map['name'] = course.courseName;
    map['days'] = course.numberOfDay;
    map['image'] = course.image;
    return map;
  }

  Course.fromMap(Map<String, dynamic> map) {
    this.courseId = map['id'];
    this.courseName = map['name'];
    this.numberOfDay = map['days'];
    this.image = map['images'];
  }
}
