import 'package:first_project/storge/helpers/db_helper.dart';

class Student {
  late int studentId;
  late String studentName;
  late int courseID;
  int attendees = 0;
  int gender = 0;
  String studentImage = '';

  Student();

  Map<String, dynamic> toMap() {
    return {
      'name': this.studentName,
      'attendees': this.attendees,
      'courseId': this.courseID,
      'gender': this.gender,
    };
  }

  Student.fromMap(Map<String, dynamic> map) {
    this.studentId = map['id'];
    this.studentName = map['name'];
    this.attendees = map['attendees'];
    this.courseID = map['courseId'];
    this.gender = map['gender'];
  }
}
