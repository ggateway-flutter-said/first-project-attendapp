import 'package:first_project/utils/app_colors.dart';
import 'package:first_project/utils/size_config.dart';
import 'package:flutter/material.dart';

import 'app_text_widget.dart';


class OnBoardingWidgets extends StatelessWidget {
  final String image;
  final String title;
  final String message;

  OnBoardingWidgets({
    required this.image,
    required this.title,
    required this.message,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: SizeConfig.scaleHeight(210),),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Image.asset('assets/images/$image'),
          SizedBox(
            height: SizeConfig.scaleHeight(36),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: SizeConfig.scaleWidth(34)),
            child: AppTextWidget(
              title,
              family: 'Cairo',
              size: SizeConfig.scaleTextFont(20),
              fontWeight: FontWeight.bold,
              textAlign: TextAlign.center,
            ),
          ),
          SizedBox(
            height: SizeConfig.scaleHeight(20),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: SizeConfig.scaleWidth(34)),
            child: AppTextWidget(
              message,
              size: SizeConfig.scaleTextFont(17),
              fontWeight: FontWeight.w500,
              textAlign: TextAlign.center,
              color: AppColors.SUB_TITLE_TEXT_COLOR,
            ),
          ),
        ],
      ),
    );
  }
}
