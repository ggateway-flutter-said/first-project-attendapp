import 'package:first_project/utils/size_config.dart';
import 'package:flutter/material.dart';

import 'app_text_widget.dart';

class SacondaryButton extends StatelessWidget {

  final void Function() onPressed;
  final String label;

  SacondaryButton({required this.onPressed, required this.label});

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(
        gradient: LinearGradient(
            colors: [
              Color(0xFF8465FF),
              Color(0xFF5933F1),
            ],
            begin: AlignmentDirectional.topStart,
            end: AlignmentDirectional.bottomEnd),
        borderRadius: BorderRadius.circular(10),
      ),
      child: ElevatedButton(
        onPressed:() {
          onPressed();
        },
        child: AppTextWidget(
          label,
          color: Colors.white,
          size: SizeConfig.scaleTextFont(15),
          family: 'redHatDisplay',
          fontWeight: FontWeight.bold,
        ),
        style: ElevatedButton.styleFrom(
          shadowColor: Colors.transparent,
          primary: Colors.transparent,
          elevation: 0,
          minimumSize: Size(0, 45),
        ),
      ),
    );
  }
}

