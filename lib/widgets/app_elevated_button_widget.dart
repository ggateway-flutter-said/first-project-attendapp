import 'package:first_project/utils/app_colors.dart';
import 'package:first_project/utils/size_config.dart';
import 'package:flutter/material.dart';

import 'app_text_widget.dart';

class AppElevatedButtonWidget extends StatelessWidget {
  final void Function() onPressed;
  final String label;

  AppElevatedButtonWidget({required this.onPressed, required this.label});

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(
        gradient: LinearGradient(
            colors: [
              AppColors.GRADIENT_BEGIN_COLOR,
              AppColors.GRADIENT_END_COLOR,
            ],
            begin: AlignmentDirectional.topStart,
            end: AlignmentDirectional.bottomEnd),
        borderRadius: BorderRadius.circular(20),
      ),
      child: ElevatedButton(
        onPressed:() {
          onPressed();
        },
        child: AppTextWidget(
          label,
          color: Colors.white,
          size: SizeConfig.scaleTextFont(18),
          fontWeight: FontWeight.bold,
        ),
        style: ElevatedButton.styleFrom(
          shadowColor: Colors.transparent,
          primary: Colors.transparent,
          elevation: 0,
          minimumSize: Size(double.infinity, SizeConfig.scaleWidth(58)),
        ),
      ),
    );
  }
}
