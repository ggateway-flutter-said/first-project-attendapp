import 'package:flutter/material.dart';

class AppTextWidget extends StatelessWidget {
  final String text;
  final String family;
  final double size;
  final FontWeight fontWeight;
  final Color color;
  final FontStyle fontStyle;
  final TextAlign textAlign;

  AppTextWidget(
    this.text, {
    this.family = 'redHatDisplay',
    this.size = 15,
    this.fontWeight = FontWeight.normal,
    this.color = Colors.black,
    this.fontStyle = FontStyle.normal,
    this.textAlign = TextAlign.start,
  });

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      textAlign: textAlign,
      style: TextStyle(
          fontSize: size,
          color: color,
          fontWeight: fontWeight,
          fontFamily: family,
          fontStyle: fontStyle,
      ),
    );
  }
}
