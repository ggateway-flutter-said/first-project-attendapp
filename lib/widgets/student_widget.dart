import 'package:first_project/models/course.dart';
import 'package:first_project/models/student.dart';
import 'package:first_project/providers/student_change_notifire.dart';
import 'package:first_project/screens/edit_student_screen.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:first_project/widgets/app_text_widget.dart';
import 'package:first_project/widgets/secondary_button_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:provider/provider.dart';

class StudentWidget extends StatelessWidget {
  final Student student;
  final Course course;

  StudentWidget({
    required this.student,
    required this.course,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        margin: EdgeInsets.all(20),
        padding: EdgeInsets.symmetric(horizontal: 25, vertical: 10),
        decoration: BoxDecoration(
            color:
                student.gender == 0 ? Colors.blue.shade50 : Colors.pink.shade50,
            borderRadius: BorderRadius.circular(20),
            boxShadow: [
              BoxShadow(
                  color: Colors.black.withOpacity(0.16),
                  blurRadius: 4,
                  spreadRadius: 2,
                  offset: Offset(3, 3))
            ]),
        child: Column(
          children: [
            Row(
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        IconButton(
                            icon: Icon(
                              Icons.delete,
                              color: Colors.red.shade900,
                            ),
                            onPressed: () {
                              Provider.of<StudentChangeNotifier>(context,
                                      listen: false)
                                  .delete(student.studentId);
                            },
                            padding: EdgeInsets.zero,
                            constraints: BoxConstraints()),
                        IconButton(
                            icon: Icon(
                              Icons.edit,
                              color: Colors.blue.shade900,
                            ),
                            onPressed: () {
                              Navigator.push(context, MaterialPageRoute(builder: (c)=>EditUserScreen(student: student)));
                            },
                            padding: EdgeInsets.zero),
                      ],
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    AppTextWidget(
                      '${AppLocalizations.of(context)!.attend_rate} ${getPercent((course.numberOfDay)).round()}%',
                      size: 13,
                    ),
                    SizedBox(height: 5,),
                    LinearPercentIndicator(
                      padding: EdgeInsets.zero,
                      width: 120,
                      percent: (getPercent(course.numberOfDay) / 100),
                      lineHeight: 5.0,
                      linearStrokeCap: LinearStrokeCap.roundAll,
                      progressColor: student.gender == 0
                          ? Colors.blue.shade900
                          : Colors.pink.shade900,
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    AppTextWidget(
                      AppLocalizations.of(context)!.attend_days,
                      size: 13,
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text(
                          student.attendees.toString() + '/',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                              color: Colors.red.shade900),
                        ),
                        Text(
                          course.numberOfDay.toString(),
                          style: TextStyle(fontSize: 13),
                        ),
                      ],
                    ),
                  ],
                ),
                Spacer(),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    // SizedBox(height: 5,),
                    CircleAvatar(
                      radius: 40,
                      backgroundImage: AssetImage(student.gender == 0
                          ? 'assets/images/male.jpg'
                          : 'assets/images/female.jpg'),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    AppTextWidget(
                      student.studentName,
                      fontWeight: FontWeight.bold,
                    ),
                  ],
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              children: [
                Expanded(
                  child: SacondaryButton(
                    onPressed: () {
                      if (course.numberOfDay > student.attendees) {
                        Student newStudent = Student();
                        newStudent.studentId = student.studentId;
                        newStudent.studentName = student.studentName;
                        newStudent.courseID = student.courseID;
                        newStudent.studentImage = student.studentImage;
                        newStudent.gender = student.gender;
                        newStudent.attendees = student.attendees + 1;
                        Provider.of<StudentChangeNotifier>(context,
                                listen: false)
                            .update(newStudent);
                      } else {
                        print('top val');
                      }
                    },
                    label: AppLocalizations.of(context)!.attend,
                  ),
                ),
                SizedBox(
                  width: 15,
                ),
                SacondaryButton(
                  onPressed: () {
                    if (student.attendees > 0) {
                      Student newStudent = Student();
                      newStudent.studentId = student.studentId;
                      newStudent.studentName = student.studentName;
                      newStudent.courseID = student.courseID;
                      newStudent.studentImage = student.studentImage;
                      newStudent.gender = student.gender;
                      newStudent.attendees = student.attendees - 1;
                      Provider.of<StudentChangeNotifier>(context, listen: false)
                          .update(newStudent);
                    } else {
                      print('top val');
                    }
                  },
                  label: AppLocalizations.of(context)!.undo,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  double getPercent(int numberOfDate) {
    return (student.attendees / numberOfDate) * 100;
  }
}
