import 'package:first_project/utils/app_colors.dart';
import 'package:first_project/utils/size_config.dart';
import 'package:flutter/material.dart';

class OnBoardingIndicator extends StatelessWidget {

  final bool isSelected;


  OnBoardingIndicator({this.isSelected = false});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: SizeConfig.scaleHeight(10),
      width: isSelected ? SizeConfig.scaleWidth(20) : SizeConfig.scaleWidth(10),
      margin: EdgeInsetsDirectional.only(
        end: SizeConfig.scaleHeight(10),
      ),
      decoration: BoxDecoration(
        color: isSelected
            ? AppColors.INDICATOR_SELECTED_COLOR
            : AppColors.INDICATOR_DEFAULT_COLOR,
        borderRadius: BorderRadius.circular(
          SizeConfig.scaleHeight(5),
        ),
      ),
    );
  }
}
