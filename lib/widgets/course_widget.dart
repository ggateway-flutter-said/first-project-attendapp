import 'package:first_project/models/course.dart';
import 'package:first_project/providers/coures_change_notifire.dart';
import 'package:first_project/screens/edite_course_screen.dart';
import 'package:first_project/screens/student_screen.dart';
import 'package:first_project/utils/size_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'app_text_widget.dart';

class CourseWidget extends StatelessWidget {
  final Course course;

  CourseWidget({
    required this.course,
  });

  var colors = [
    Colors.green.shade50,
    Colors.red.shade50,
    Colors.yellow.shade50,
    Colors.blue.shade50,
    Colors.pink.shade50,
    Colors.purple.shade50
  ];

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        height: SizeConfig.scaleHeight(100),
        padding: EdgeInsets.symmetric(
          horizontal: SizeConfig.scaleWidth(20),
          vertical: SizeConfig.scaleHeight(5),
        ),
        margin: EdgeInsets.symmetric(
          horizontal: SizeConfig.scaleWidth(10),
          vertical: SizeConfig.scaleHeight(7),
        ),
        decoration: BoxDecoration(
          color: (colors.toList()..shuffle()).first,
          borderRadius: BorderRadius.circular(20),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.16),
              blurRadius: SizeConfig.scaleHeight(4),
              spreadRadius: SizeConfig.scaleHeight(2),
              offset: Offset(
                SizeConfig.scaleHeight(3),
                SizeConfig.scaleHeight(3),
              ),
            ),
          ],
        ),
        child: Row(
          children: [
            Image.asset(
              // 'assets/images/${course.image}.png',
              course.image ?? 'assets/images/settings.png',
              height: SizeConfig.scaleHeight(50),
              width: SizeConfig.scaleWidth(50),
              fit: BoxFit.cover,
            ),
            SizedBox(
              width: SizeConfig.scaleWidth(15),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                AppTextWidget(
                  course.courseName,
                  fontWeight: FontWeight.bold,
                  size: SizeConfig.scaleTextFont(17),
                ),
                SizedBox(
                  height: SizeConfig.scaleHeight(5),
                ),
                AppTextWidget(
                  '${AppLocalizations.of(context)!.attend_days}: ${course.numberOfDay}',
                  size: SizeConfig.scaleTextFont(13),
                  color: Colors.grey,
                  fontWeight: FontWeight.w500,
                ),
              ],
            ),
            Spacer(),
            IconButton(
              icon: Icon(
                Icons.edit,
                color: Colors.blue.shade900,
              ),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => EditCourseScreen(
                      course: course,
                    ),
                  ),
                );
              },
            ),
            IconButton(
              icon: Icon(
                Icons.delete,
                color: Colors.red.shade900,
              ),
              onPressed: () {
                Provider.of<CourseChangeNotifier>(context, listen: false)
                    .delete(course.courseId);
              },
            ),
          ],
        ),
      ),
      onTap: () => Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => StudentScreen(
            course: course,
          ),
        ),
      ),
    );
  }
}
