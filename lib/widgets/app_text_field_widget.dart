import 'package:first_project/utils/size_config.dart';
import 'package:flutter/material.dart';

class AppTextFieldWidget extends StatelessWidget {
  final bool isPassword;
  final String hint;
  final TextInputType textInputType;
  final TextEditingController? controller;
  final String? errorMsg;

  AppTextFieldWidget({
    this.isPassword = false,
    this.textInputType = TextInputType.text,
    this.controller,
    this.errorMsg,
    this.hint = '',
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: SizeConfig.scaleHeight(10)),
      child: TextField(
        controller: controller,
        obscureText: isPassword,
        keyboardType: textInputType,
        cursorColor: Colors.black,
        style: TextStyle(
            color: Colors.black,
            fontFamily: 'redHatDisplay',
            fontSize: SizeConfig.scaleTextFont(18),
            fontWeight: FontWeight.w500),
        decoration: InputDecoration(
          labelText: hint,
          errorText: errorMsg,
          errorStyle: TextStyle(
            color: Colors.red.shade500,
            fontWeight: FontWeight.w500,
            fontFamily: 'redHatDisplay',
            fontSize: SizeConfig.scaleTextFont(16),
          ),
          labelStyle: TextStyle(
            color: Colors.grey.shade500,
            fontWeight: FontWeight.w500,
            fontFamily: 'redHatDisplay',
            fontSize: SizeConfig.scaleTextFont(13),
          ),
          errorBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.red.shade300,
            ),
            borderRadius: BorderRadius.circular(
              SizeConfig.scaleHeight(15),
            ),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.grey.shade300,
            ),
            borderRadius: BorderRadius.circular(
              SizeConfig.scaleHeight(15),
            ),
          ),
          disabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.grey.shade300,
            ),
            borderRadius: BorderRadius.circular(
              SizeConfig.scaleHeight(15),
            ),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.red.shade300,
            ),
            borderRadius: BorderRadius.circular(
              SizeConfig.scaleHeight(15),
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.grey.shade500,
            ),
            borderRadius: BorderRadius.circular(
              SizeConfig.scaleHeight(15),
            ),
          ),
        ),
      ),
    );
  }
}
