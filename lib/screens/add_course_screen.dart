import 'package:first_project/models/course.dart';
import 'package:first_project/providers/coures_change_notifire.dart';
import 'package:first_project/screens/images_profile_screen.dart';
import 'package:first_project/utils/app_colors.dart';
import 'package:first_project/utils/helper.dart';
import 'package:first_project/utils/size_config.dart';
import 'package:first_project/widgets/app_elevated_button_widget.dart';
import 'package:first_project/widgets/app_text_field_widget.dart';
import 'package:first_project/widgets/app_text_widget.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class AddCourseScreen extends StatefulWidget {
  final String? image;

  AddCourseScreen({this.image});

  @override
  _AddCourseScreenState createState() => _AddCourseScreenState();
}

class _AddCourseScreenState extends State<AddCourseScreen> with Helper {
  late TextEditingController _nameEditingController;
  late TextEditingController _daysEditingController;

  String? msgNameError;
  String? msgNumberOfDayError;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _nameEditingController = TextEditingController();
    _daysEditingController = TextEditingController();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _nameEditingController.dispose();
    _daysEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.SCAFFOLD_COLOR,
      appBar: AppBar(
        title: AppTextWidget(
          AppLocalizations.of(context)!.add_new_course,
          color: Colors.white,
          fontWeight: FontWeight.bold,
          size: SizeConfig.scaleTextFont(18),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: SizeConfig.scaleWidth(30), vertical: SizeConfig.scaleHeight(10)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              GestureDetector(
                onTap: () {
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (con) => ImageProfileScreen()));
                },
                child: Container(
                  child: widget.image == null
                      ? Icon(
                          Icons.camera_alt_outlined,
                          color: Colors.white,
                          size: SizeConfig.scaleHeight(25),
                        )
                      : Container(),
                  alignment: Alignment.center,
                  width: SizeConfig.scaleWidth(100),
                  height: SizeConfig.scaleHeight(100),
                  margin: EdgeInsetsDirectional.only(
                    top: SizeConfig.scaleHeight(40),
                    bottom: SizeConfig.scaleHeight(20),
                  ),
                  decoration: BoxDecoration(
                      color: Colors.black,
                      image: widget.image == null
                          ? DecorationImage(
                              colorFilter: new ColorFilter.mode(
                                  Colors.black.withOpacity(0.3),
                                  BlendMode.dstATop),
                              image: AssetImage(
                                  widget.image ?? 'assets/images/settings.png'),
                              fit: BoxFit.cover,
                            )
                          : DecorationImage(
                              image: AssetImage(
                                  widget.image ?? 'assets/images/settings.png'),
                              fit: BoxFit.cover,
                            ),
                      shape: BoxShape.circle),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              AppTextFieldWidget(
                hint: AppLocalizations.of(context)!.hint_text_field_name_course,
                controller: _nameEditingController,
                textInputType: TextInputType.text,
                errorMsg: msgNameError,
              ),
              AppTextFieldWidget(
                hint: AppLocalizations.of(context)!.hint_text_field_days_course,
                controller: _daysEditingController,
                textInputType: TextInputType.number,
                errorMsg: msgNumberOfDayError,
              ),
              SizedBox(
                height: 15,
              ),
              AppElevatedButtonWidget(
                onPressed: performSave,
                label: AppLocalizations.of(context)!.save,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future performSave() async {
    if (checkData()) {
      await saveCourse();
      Navigator.pop(context);
    }
  }

  bool checkData() {
    if (_nameEditingController.text.isNotEmpty &&
        _daysEditingController.text.isNotEmpty) {
      checkErrors();
      return true;
    }
    checkErrors();
    showSnackBar(context,
        text: AppLocalizations.of(context)!.msg_error_empty_field, error: true);
    return false;
  }

  Future saveCourse() async {
    bool inserted =
        await Provider.of<CourseChangeNotifier>(context, listen: false)
            .create(course);
    if (inserted) {
      showSnackBar(context,
          text: AppLocalizations.of(context)!.success_msg_add_Course);
      clear();
    } else {
      showSnackBar(context,
          text: AppLocalizations.of(context)!.failed_msg_add_Course,
          error: true);
    }
  }

  Course get course {
    Course course = Course();
    course.courseName = _nameEditingController.text;
    course.numberOfDay = int.parse(_daysEditingController.text);
    course.image = widget.image ?? 'assets/images/settings.png';
    return course;
  }

  void clear() {
    _nameEditingController.text = '';
    _daysEditingController.text = '';
  }

  void checkErrors() {
    setState(() {
      msgNameError = _nameEditingController.text.isEmpty
          ? AppLocalizations.of(context)!.msg_error_text_field_name_course
          : null;
      msgNumberOfDayError = _daysEditingController.text.isEmpty
          ? AppLocalizations.of(context)!.msg_error_text_field_days_course
          : null;
    });
  }
}
