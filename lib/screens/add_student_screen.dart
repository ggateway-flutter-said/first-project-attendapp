import 'package:first_project/models/course.dart';
import 'package:first_project/models/student.dart';
import 'package:first_project/providers/coures_change_notifire.dart';
import 'package:first_project/providers/student_change_notifire.dart';
import 'package:first_project/utils/app_colors.dart';
import 'package:first_project/utils/helper.dart';
import 'package:first_project/utils/size_config.dart';
import 'package:first_project/widgets/app_elevated_button_widget.dart';
import 'package:first_project/widgets/app_text_field_widget.dart';
import 'package:first_project/widgets/app_text_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class AddStudentScreen extends StatefulWidget {
  final Course course;

  AddStudentScreen({required this.course});

  @override
  _AddStudentScreenState createState() => _AddStudentScreenState();
}

class _AddStudentScreenState extends State<AddStudentScreen> with Helper {
  late TextEditingController _nameEditingController;
  int gender = 0;
  int groupValue = 0;
  String? msgNameError;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _nameEditingController = TextEditingController();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _nameEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.SCAFFOLD_COLOR,
      appBar: AppBar(
        title: AppTextWidget(
          AppLocalizations.of(context)!.add_student_appbar,
          color: Colors.white,
          fontWeight: FontWeight.w500,
          size: SizeConfig.scaleTextFont(18),
        ),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: SizeConfig.scaleWidth(30),
              vertical: SizeConfig.scaleHeight(30)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                AppLocalizations.of(context)!.add_new_student,
                style: TextStyle(
                  color: Colors.black,
                  fontSize: SizeConfig.scaleTextFont(25),
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 20),
              AppTextFieldWidget(
                controller: _nameEditingController,
                textInputType: TextInputType.text,
                hint: AppLocalizations.of(context)!.student_hint_name,
                errorMsg: msgNameError,
              ),
              SizedBox(height: SizeConfig.scaleHeight(10)),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Expanded(
                    child: RadioListTile<int>(
                      value: 0,
                      groupValue: groupValue,
                      onChanged: (val) {
                        setState(() {
                          groupValue = val!;
                        });
                      },
                      title: AppTextWidget(
                        AppLocalizations.of(context)!.male,
                      ),
                    ),
                  ),
                  Expanded(
                    child: RadioListTile<int>(
                      value: 1,
                      groupValue: groupValue,
                      onChanged: (val) {
                        setState(() {
                          groupValue = val!;
                        });
                      },
                      title: AppTextWidget(
                        AppLocalizations.of(context)!.female,
                      ),
                    ),
                  ),
                ],
              ),
              // SizedBox(height: SizeConfig.scaleHeight(50)),
              AppElevatedButtonWidget(
                  onPressed: performSave,
                  label: AppLocalizations.of(context)!.save)
            ],
          ),
        ),
      ),
    );
  }

  Future performSave() async {
    if (checkData()) {
      await saveCourse();
      Navigator.pop(context);
    }
  }

  bool checkData() {
    if (_nameEditingController.text.isNotEmpty) {
      checkErrors();
      return true;
    }

    checkErrors();
    showSnackBar(context,
        text: AppLocalizations.of(context)!.msg_error_empty_field, error: true);
    return false;
  }

  Future saveCourse() async {
    bool inserted =
        await Provider.of<StudentChangeNotifier>(context, listen: false)
            .create(student);
    if (inserted) {
      showSnackBar(context,
          text: AppLocalizations.of(context)!.success_msg_add_student);
      clear();
    } else {
      showSnackBar(context,
          text: AppLocalizations.of(context)!.failed_msg_add_student,
          error: true);
    }
  }

  Student get student {
    Student student = Student();
    student.studentName = _nameEditingController.text;
    student.courseID = widget.course.courseId;
    student.gender = groupValue;
    return student;
  }

  void clear() {
    _nameEditingController.text = '';
  }

  void checkErrors() {
    setState(() {
      msgNameError = _nameEditingController.text.isEmpty
          ? AppLocalizations.of(context)!.msg_error_text_field_name_student
          : null;
    });
  }
}
