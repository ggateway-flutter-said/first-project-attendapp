import 'package:first_project/models/course.dart';
import 'package:first_project/models/student.dart';
import 'package:first_project/providers/student_change_notifire.dart';
import 'package:first_project/screens/add_student_screen.dart';
import 'package:first_project/utils/size_config.dart';
import 'package:first_project/widgets/app_text_widget.dart';
import 'package:first_project/widgets/student_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class StudentScreen extends StatelessWidget {
  final Course course;

  StudentScreen({required this.course});

  @override
  Widget build(BuildContext context) {
    List<Student> students = Provider.of<StudentChangeNotifier>(context)
        .students
        .where((Student student) => student.courseID == course.courseId)
        .toList();

    return Scaffold(
      appBar: AppBar(
        title: AppTextWidget(
          AppLocalizations.of(context)!.students_appbar_text,
          color: Colors.white,
          fontWeight: FontWeight.w500,
          size: SizeConfig.scaleTextFont(18),
        ),
        centerTitle: true,
      ),
      body: students.length == 0
          ? Center(
              child: AppTextWidget(
                AppLocalizations.of(context)!.no_data,
                size: SizeConfig.scaleTextFont(13),
                fontWeight: FontWeight.w300,
                color: Colors.grey,
              ),
            )
          : ListView.builder(
              itemCount: students.length,
              itemBuilder: (context, index) {
                Student student = students[index];
                return StudentWidget(
                  student: student,
                  course: course,
                );
              },
            ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => AddStudentScreen(course: course),
          ),
        ),
        child: Icon(Icons.add),
      ),
    );
  }
}
