import 'package:first_project/models/student.dart';
import 'package:first_project/providers/student_change_notifire.dart';
import 'package:first_project/utils/app_colors.dart';
import 'package:first_project/utils/helper.dart';
import 'package:first_project/utils/size_config.dart';
import 'package:first_project/widgets/app_elevated_button_widget.dart';
import 'package:first_project/widgets/app_text_field_widget.dart';
import 'package:first_project/widgets/app_text_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

class EditUserScreen extends StatefulWidget {

  final Student student;

  EditUserScreen({required this.student});

  @override
  _EditUserScreenState createState() => _EditUserScreenState();
}

class _EditUserScreenState extends State<EditUserScreen> with Helper{

  late TextEditingController _nameEditingController;
  String? msgNameError;

  @override
  void initState() {
    super.initState();
    _nameEditingController = TextEditingController(text: widget.student.studentName);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _nameEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.SCAFFOLD_COLOR,
      appBar: AppBar(
        title: AppTextWidget(
          AppLocalizations.of(context)!.edit_student,
          color: Colors.white,
          fontWeight: FontWeight.w500,
          size: SizeConfig.scaleTextFont(18),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              AppTextFieldWidget(
                hint: AppLocalizations.of(context)!.hint_text_field_name_course,
                controller: _nameEditingController,
                textInputType: TextInputType.text,
                errorMsg: msgNameError,
              ),
              SizedBox(
                height: 15,
              ),
              AppElevatedButtonWidget(
                onPressed: performEdit,
                label: AppLocalizations.of(context)!.save,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future performEdit() async {
    if (checkData()) {
      await saveCourse();
      Navigator.pop(context);
    }
  }

  bool checkData() {
    if (_nameEditingController.text.isNotEmpty ) {
      checkErrors();
      return true;
    }
    checkErrors();
    showSnackBar(
      context,
      text: AppLocalizations.of(context)!.msg_error_empty_field,
      error: true,
    );
    return false;
  }

  Future saveCourse() async {
    bool inserted =
    await Provider.of<StudentChangeNotifier>(context, listen: false)
        .update(student);
    if (inserted) {
      showSnackBar( context,
          text: AppLocalizations.of(context)!.success_msg_add_student);
      clear();
    } else {
      showSnackBar(
          context,
          text: AppLocalizations.of(context)!.failed_msg_add_student,
          error: true);
    }
  }

  Student get student {
    Student student = widget.student;
    student.studentName = _nameEditingController.text;
    return student;
  }

  void clear() {
    _nameEditingController.text = '';
  }

  void checkErrors() {
    setState(() {
      msgNameError = _nameEditingController.text.isEmpty
          ? AppLocalizations.of(context)!.msg_error_text_field_name_student
          : null;
    });
  }
}
