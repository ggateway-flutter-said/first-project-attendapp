import 'package:first_project/providers/settings_change-notifire.dart';
import 'package:first_project/storge/helpers/shared_pref_helper.dart';
import 'package:first_project/utils/size_config.dart';
import 'package:first_project/widgets/app_elevated_button_widget.dart';
import 'package:first_project/widgets/app_text_widget.dart';
import 'package:first_project/widgets/on_boarding_indicator_widget.dart';
import 'package:first_project/widgets/on_boarding_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';


class OnBoardingScreen extends StatefulWidget {
  @override
  _OnBoardingScreenState createState() => _OnBoardingScreenState();
}

class _OnBoardingScreenState extends State<OnBoardingScreen> {
  late PageController _pageController;
  int currantPage = 0;

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          PageView(
            controller: _pageController,
            onPageChanged: (int selectedPage) {
              setState(() {
                currantPage = selectedPage;
              });
            },
            children: [
              OnBoardingWidgets(
                image: 'p2.jpg',
                title: AppLocalizations.of(context)!.title_on_boarding_screen_one,
                message:AppLocalizations.of(context)!.msg_on_boarding_screen_one,
              ),
              OnBoardingWidgets(
                image: 'p3.png',
                title: AppLocalizations.of(context)!.title_on_boarding_screen_tow,
                message: AppLocalizations.of(context)!.msg_on_boarding_screen_tow,
              ),
              OnBoardingWidgets(
                image: 'p4.jpg',
                title: AppLocalizations.of(context)!.title_on_boarding_screen_three,
                message:AppLocalizations.of(context)!.msg_on_boarding_screen_three
              ),
            ],
          ),
          PositionedDirectional(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                OnBoardingIndicator(
                  isSelected: currantPage == 0,
                ),
                OnBoardingIndicator(
                  isSelected: currantPage == 1,
                ),
                OnBoardingIndicator(
                  isSelected: currantPage == 2,
                ),
              ],
            ),
            bottom: SizeConfig.scaleHeight(155),
            start: 0,
            end: 0,
          ),
          PositionedDirectional(
            top: SizeConfig.scaleHeight(77),
            end: SizeConfig.scaleWidth(30),
            child: Visibility(
              visible: currantPage != 2,
              child: TextButton(
                onPressed: () => _pageController.jumpToPage(2),
                child: AppTextWidget(AppLocalizations.of(context)!.skip),
              ),
            ),
          ),
          Visibility(
            visible: currantPage == 2,
            child: PositionedDirectional(
              bottom: SizeConfig.scaleHeight(60),
              start: SizeConfig.scaleWidth(36),
              end: SizeConfig.scaleWidth(36),
              child: AppElevatedButtonWidget(
                onPressed: () {
                  // AppPrefController().setLoginState(false);
                  Navigator.pushReplacementNamed(context, '/course_screen');
                },
                label: AppLocalizations.of(context)!.start,

              ),
            ),
          ),
        ],
      ),
    );
  }
}
