import 'package:first_project/screens/add_course_screen.dart';
import 'package:first_project/utils/size_config.dart';
import 'package:first_project/widgets/app_text_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ImageProfileScreen extends StatelessWidget {
  List<String> images = [
    'assets/images/android.png',
    'assets/images/e_marketing.png',
    'assets/images/graphic_designer.png',
    'assets/images/laravel.png',
    'assets/images/translate.png',
    'assets/images/ux.png',
    'assets/images/wordpress.png',
    'assets/images/ios.png',
    'assets/images/accounting.png',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: AppTextWidget(
          AppLocalizations.of(context)!.choice_image,
          color: Colors.white,
          fontWeight: FontWeight.w500,
          size: SizeConfig.scaleTextFont(18),
        ),
      ),
      body: Padding(
        padding: EdgeInsets.all(25),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: SizeConfig.scaleHeight(50),
            ),
            AppTextWidget(
                AppLocalizations.of(context)!.choice_image,
              size: SizeConfig.scaleTextFont(18),
            ),
            SizedBox(
              height: SizeConfig.scaleHeight(50),
            ),
            Expanded(
              child: GridView.builder(
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3,
                      mainAxisSpacing: 10,
                      crossAxisSpacing: 15,
                      childAspectRatio: 2 / 2),
                  itemCount: images.length,
                  itemBuilder: (context, index) {
                    String image = images[index];
                    return GestureDetector(
                      onTap: () {
                        Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    AddCourseScreen(image: image)));
                      },
                      child: Container(
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage(
                          images[index],
                        ))),
                      ),
                    );
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
