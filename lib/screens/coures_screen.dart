import 'package:first_project/models/course.dart';
import 'package:first_project/providers/coures_change_notifire.dart';
import 'package:first_project/providers/settings_change-notifire.dart';
import 'package:first_project/utils/app_colors.dart';
import 'package:first_project/utils/size_config.dart';
import 'package:first_project/widgets/app_text_widget.dart';
import 'package:first_project/widgets/course_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class CourseScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.SCAFFOLD_COLOR,
      appBar: AppBar(
        title: AppTextWidget(
          AppLocalizations.of(context)!.course_appbar_text,
          color: Colors.white,
          fontWeight: FontWeight.bold,
          size: SizeConfig.scaleTextFont(18),
        ),
        centerTitle: true,
        actions: [
          IconButton(
            onPressed: () {
              Locale currentLocale = Localizations.localeOf(context);
              String newLocaleLanguage = currentLocale.languageCode == 'ar' ? 'en' : 'ar';
              Provider.of<SettingsChangeNotifier>(context, listen: false).changeLang(newLocaleLanguage);
            },
            icon: Icon(Icons.language),
          )
        ],
      ),
      body: Consumer<CourseChangeNotifier>(
        builder: (
          BuildContext context,
          CourseChangeNotifier value,
          Widget? child,
        ) {
          if (value.courses.isEmpty) {
            return Center(
              child: AppTextWidget(
                AppLocalizations.of(context)!.no_data,
                size: SizeConfig.scaleTextFont(13),
                fontWeight: FontWeight.w300,
                color: Colors.grey,
              ),
            );
          }

          return ListView.builder(itemCount: value.courses.length ,itemBuilder: (context, index){
            Course course = value.courses[index];
            return CourseWidget(course: course,);
          });
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => Navigator.pushNamed(context, '/add_course_screen'),
        child: Icon(Icons.add),
      ),
    );
  }
}
