import 'package:first_project/models/course.dart';
import 'package:first_project/providers/coures_change_notifire.dart';
import 'package:first_project/utils/app_colors.dart';
import 'package:first_project/utils/helper.dart';
import 'package:first_project/utils/size_config.dart';
import 'package:first_project/widgets/app_elevated_button_widget.dart';
import 'package:first_project/widgets/app_text_field_widget.dart';
import 'package:first_project/widgets/app_text_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

class EditCourseScreen extends StatefulWidget  with Helper {

  final Course course;

  EditCourseScreen({required this.course});

  @override
  _EditCourseScreenState createState() => _EditCourseScreenState();
}

class _EditCourseScreenState extends State<EditCourseScreen> with Helper{

  late TextEditingController _nameEditingController;
  late TextEditingController _daysEditingController;

  String? msgNameError;
  String? msgNumberOfDayError;

  @override
  void initState() {
    super.initState();
    _nameEditingController = TextEditingController(text: widget.course.courseName);
    _daysEditingController = TextEditingController(text: widget.course.numberOfDay.toString());
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _nameEditingController.dispose();
    _daysEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      backgroundColor: AppColors.SCAFFOLD_COLOR,
      appBar: AppBar(
        title: AppTextWidget(
          AppLocalizations.of(context)!.edit_course,
          color: Colors.white,
          fontWeight: FontWeight.w500,
          size: SizeConfig.scaleTextFont(18),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              AppTextFieldWidget(
                hint: AppLocalizations.of(context)!.hint_text_field_name_course,
                controller: _nameEditingController,
                textInputType: TextInputType.text,
                errorMsg: msgNameError,
              ),
              AppTextFieldWidget(
                hint: AppLocalizations.of(context)!.hint_text_field_days_course,
                controller: _daysEditingController,
                textInputType: TextInputType.number,
                errorMsg: msgNumberOfDayError,
              ),
              SizedBox(
                height: 15,
              ),
              AppElevatedButtonWidget(
                onPressed: performEdit,
                label: AppLocalizations.of(context)!.save,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future performEdit() async {
    if (checkData()) {
      await saveCourse();
      Navigator.pop(context);
    }
  }

  bool checkData() {
    if (_nameEditingController.text.isNotEmpty &&
        _daysEditingController.text.isNotEmpty) {
      checkErrors();
      return true;
    }
    checkErrors();
   showSnackBar(
      context,
      text: AppLocalizations.of(context)!.msg_error_empty_field,
      error: true,
    );
    return false;
  }

  Future saveCourse() async {
    bool inserted =
    await Provider.of<CourseChangeNotifier>(context, listen: false)
        .update(course);
    if (inserted) {
      showSnackBar( context,
          text: AppLocalizations.of(context)!.success_msg_update_student);
      clear();
    } else {
      showSnackBar(
           context,
          text: AppLocalizations.of(context)!.failed_msg_add_Course,
          error: true);
    }
  }

  Course get course {
    Course course = widget.course;
    course.courseName = _nameEditingController.text;
    course.numberOfDay = int.parse(_daysEditingController.text);
    return course;
  }

  void clear() {
    _nameEditingController.text = '';
    _daysEditingController.text = '';
  }

  void checkErrors() {
    setState(() {
      msgNameError = _nameEditingController.text.isEmpty
          ? AppLocalizations.of(context)!.msg_error_text_field_name_course
          : null;
      msgNumberOfDayError = _daysEditingController.text.isEmpty
          ? AppLocalizations.of(context)!.failed_msg_add_Course
          : null;
    });
  }
}
