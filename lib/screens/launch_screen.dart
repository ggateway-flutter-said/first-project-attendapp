import 'package:first_project/storge/helpers/shared_pref_helper.dart';
import 'package:first_project/utils/app_colors.dart';
import 'package:first_project/utils/size_config.dart';
import 'package:first_project/widgets/app_text_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class LaunchScreen extends StatefulWidget {
  @override
  _LaunchScreenState createState() => _LaunchScreenState();
}

class _LaunchScreenState extends State<LaunchScreen> {
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 3), () {
      if (SharedPrefHelper().loginState)
        Navigator.pushReplacementNamed(context, '/on_boarding_screen');
      else
        Navigator.pushReplacementNamed(context, '/course_screen');

    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return  Scaffold(
      body: Container(
        alignment: AlignmentDirectional.center,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: AlignmentDirectional.topStart,
            end: AlignmentDirectional.bottomEnd,
            colors: [
              AppColors.GRADIENT_BEGIN_COLOR,
              AppColors.GRADIENT_END_COLOR
            ],
          ),
        ),
        child: AppTextWidget(
          'Attend App',
          color: Colors.white,
          fontWeight: FontWeight.bold,
          family: 'Dancing',
          size: SizeConfig.scaleTextFont(50),
        ),
      ),
    );
  }
}

